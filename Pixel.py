#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Pixel(object):
    
    def __init__(self, r=0, g=0, b=0, i=0, up=False):
        self.i, self.up = i, up

        # if no rgb was given, make it white. this makes sure that
        # if we were init'ed with rgb values they are shown correctly
        # instead of adding the other colors in
        if not r and not g and not b:
            self.r = self.g = self.b = 255
        else:
            self.r, self.g, self.b = r, g, b

        # make sure we're going off in the right direction
        if self.i < 1:  # if we're at the bottom end
            self.up = True
        elif 7 < self.i:  # if we're at the top end
            self.up = False


    def move(self, pixels):

        # if we're going up
        if self.up:
            # if the next spot up is already busy
            if any([p.i == self.i + 1 for p in pixels]):
                self.up = False  # go down instead of up
            else:
                self.i = self. i + 1

        else:
            # if the next spot down is already busy
            if any([p.i == self.i - 1 for p in pixels]):
                self.up = True  # go up instead of down
            else:
                self.i = self.i - 1

        if self.i not in range(1, 7):
            self.up = not self.up

        print(self.status())

    def status(self):
        return self.i, self.up
