#!/usr/bin/env python
# -*- coding: utf-8 -*-

import blinkt
import random
import time

from Pixel import Pixel

blinkt.set_brightness(0.1)

def main():
    pixels = [
        Pixel(r=255),
        Pixel(i=5, b=255),
        Pixel(i=3, g=255),
    ]
    
    while True:
        blinkt.clear()

        for p in pixels:
            blinkt.set_pixel(p.i, p.r, p.g, p.b)
            p.move(pixels)

        blinkt.show()
        time.sleep(.03)

if __name__ == "__main__":
    main()
